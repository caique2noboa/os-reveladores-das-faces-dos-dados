# Os Reveladores Das Faces Dos Dados

## Membros
- Bruno Souza Zabot
    - RA: 2238039
    - user: bzabot
    - e-mail: zabot@alunos.utfpr.edu.br
    - curso: Eng. Mecânica
    - UTFPR

- Caique Salvador Noboa
    - RA: 1904949
    - user: caique2noboa
    - e-mail: noboa@alunos.utfpr.edu.br
    - curso: Eng. Comp
    - UTFPR

- Maria Gabriela Rodrigues Policarpo
    - RA: 2450151
    - user: mariagabrielapolicarpo
    - e-mail: mariagabrielapolicarpo@alunos.utfpr.edu.br
    - curso: BSI
    - UTFPR

- Moisés Bryan Carneiro Roja
    - RA: 2243814
    - user: moisesbryan
    - e-mail: moisesbryan@alunos.utfpr.edu.br
    - curso: Engenharia Mecatrônica
    - UTFPR


## Tema
Entendendo fatores que podem influenciar o preço dos combustíveis dos estados brasileiros. Período de 2004 a 2021.

## Objetivos
- Relacionar o preço médio do barril de petróleo mundial em dólar ao valor dos combustíveis em reais de cada estado.
- Compreender se o etanol é mais barato nos estados que produzem esse combustível.
- Compreender o quanto acontecimentos marcantes internacionais e nacionais influenciaram o preço do combustível, como a quebra da bolsa de valores em 2008 e o impeachment em 2016.
- Compreender a motivação da greve dos caminhoneiros de 2018, que alegavam que o preço do óleo diesel havia subido mais de 50% no período de 1 ano. (fonte: Greve dos caminhoneiros: a cronologia dos 10 dias que pararam o Brasil - 30/05/2018 - UOL Economia)
- Informar visualmente a quantidade de litros de petróleo possível de se comprar com um salário mínimo em cada ano.
- Entender como a pandemia influenciou o preço dos combustíveis nos estados.

## Fontes de dados
- Preço dos diferentes tipos de gás dos estados do Brasil de cada mês desde 2004 a abril de 2021: https://www.kaggle.com/datasets/matheusfreitag/gas-prices-in-brazil

- Preço do barril do petróleo https://www.kaggle.com/datasets/mabusalah/brent-oil-prices

- Valor do dólar em reais https://br.investing.com/currencies/usd-brl-historical-data

- Evolução do salário mínimo no Brasil: https://audtecgestao.com.br/capa.asp?infoid=1336

- Mudanças na mobilidade por conta da pandemia https://www.google.com/covid19/mobility/

## Desafios
O desafio será entender as motivações pelas quais o preço variou ao longo dos anos, se foi necessariamente o aumento do dólar que influenciou, as diferenças de preços de combustíveis entre estados da mesma região, se um estado pode ter um preço menor por produzir esse combustível etc.
